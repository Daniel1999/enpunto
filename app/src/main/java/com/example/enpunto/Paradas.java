package com.example.enpunto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Paradas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paradas);
    }

    public void Plaza_Xalisco (View view){
        Intent plaza = new Intent(this, Plaza_Xalisco.class);
        startActivity(plaza);
    }

    public void Conchita (View view){
        Intent conchita = new Intent(this, Conchita.class);
        startActivity(conchita);
    }

    public void Avenida_S (View view){
        Intent avenida = new Intent(this, Avenida_sol.class);
        startActivity(avenida);
    }

    public void Cruz (View view){
        Intent cruz = new Intent(this, Cruz.class);
        startActivity(cruz);
    }

    public void Soriana (View view){
        Intent soriana = new Intent(this, Soriana.class);
        startActivity(soriana);
    }

    public void More_Vera (View view){
        Intent morelia = new Intent(this, Mor_Vera.class);
        startActivity(morelia);
    }

    public void Ut (View view){
        Intent ut = new Intent(this, Ut.class);
        startActivity(ut);
    }
}
