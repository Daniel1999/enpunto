package com.example.enpunto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Comentarios extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comentarios);
    }

    public void Comentario (View view){
        Intent comentario = new Intent(this, Comentario.class);
        startActivity(comentario);
    }

    public void Denuncia (View view){
        Intent denuncia = new Intent(this, Denuncia.class);
        startActivity(denuncia);
    }
}
