package com.example.enpunto;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BasedeDatos extends SQLiteOpenHelper {
    public BasedeDatos(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
}
    @Override
    public void onCreate(SQLiteDatabase BasedeDatos){
        BasedeDatos.execSQL("create table comentarios (Usuario text, Comentario text)");
        BasedeDatos.execSQL("create table denuncia (Usuario text, Lugar text, Descripcion texto)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int il){

    }
}
