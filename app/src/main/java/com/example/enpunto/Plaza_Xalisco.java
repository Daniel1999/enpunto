package com.example.enpunto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Plaza_Xalisco extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plaza__xalisco);
    }
    public void Xalisco (View view){
        Intent xalisco = new Intent(this, Mapa_plaxaxalisco.class);
        startActivity(xalisco);
    }
}
