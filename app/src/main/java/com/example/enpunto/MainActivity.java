package com.example.enpunto;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    final private int REQUEST_CODE_ASK_PERMISSION = 124;
    int hasInternetPermission;


    private void accesPermission(){
        hasInternetPermission = checkSelfPermission(Manifest.permission.INTERNET);
        if (hasInternetPermission != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]{Manifest.permission.INTERNET},
            REQUEST_CODE_ASK_PERMISSION);
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        accesPermission();
    }

    public void onRequestPermissionResults(int requestCode, String[] permission, int[] grandResults){
        switch (requestCode){
            case REQUEST_CODE_ASK_PERMISSION:
                if(grandResults[0] == PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(MainActivity.this, "Permiso garantizado para conexion a internet.",Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(MainActivity.this, "Permiso denegado. Si requiere el buen funcionamiento de la aplicacion debe de permitir la conexion a internet", Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permission, grandResults);
        }

    }
    public void Mapa(View view){
        Intent mapa = new Intent(this, Ruta.class);
        startActivity(mapa);
    }

    public void Paradas (View view){
        Intent parada = new Intent(this, Paradas.class);
        startActivity(parada);
    }

    public void Comentarios (View view){
        Intent comentarios = new Intent(this, Comentarios.class);
        startActivity(comentarios);
    }


}
