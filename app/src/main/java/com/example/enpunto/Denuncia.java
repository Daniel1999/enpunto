package com.example.enpunto;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Denuncia extends AppCompatActivity {
    private EditText edt_usuario, edt_lugar, edt_descripcion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_denuncia);

        edt_usuario = (EditText)findViewById(R.id.txusuario);
        edt_lugar = (EditText)findViewById(R.id.txlugar);
        edt_descripcion= (EditText)findViewById(R.id.txdescripcion);
    }

    public void denuncia(View view){
        BasedeDatos base = new BasedeDatos(this, "denuncia", null, 1);
        SQLiteDatabase sqLiteDatabase = base.getWritableDatabase();

        String usuario = edt_usuario.getText().toString();
        String lugar = edt_lugar.getText().toString();
        String descripcion = edt_descripcion.getText().toString();

        if (!usuario.isEmpty() && !lugar.isEmpty() && !descripcion.isEmpty()){
            ContentValues  denuncia = new ContentValues();
            denuncia.put("Usuario", usuario);
            denuncia.put("Lugar", lugar);
            denuncia.put("Descripcion",descripcion);

            sqLiteDatabase.insert("denuncia", null, denuncia);

            sqLiteDatabase.close();
            edt_usuario.setText("");
            edt_lugar.setText("");
            edt_descripcion.setText("");

            Toast.makeText(this, "Denuncia realizada", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Debes de llenar todos los campos", Toast.LENGTH_SHORT).show();
        }
    }
}
