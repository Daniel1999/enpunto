package com.example.enpunto;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class Comentario extends AppCompatActivity {
    private EditText edt_usuario, edt_comentario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comentario);

        edt_usuario = (EditText)findViewById(R.id.txtusuario);
        edt_comentario = (EditText)findViewById(R.id.txtcomentario);
    }

    public  void Comentar (View view) {
    BasedeDatos base = new BasedeDatos(this, "comentarios", null, 1);
        SQLiteDatabase sqLiteDatabase = base.getWritableDatabase();

        String usuario = edt_usuario.getText().toString();
        String comentario = edt_comentario.getText().toString();

        if (!usuario.isEmpty() && !comentario.isEmpty()){
            ContentValues comentar = new ContentValues();
            comentar.put("Usuario",usuario);
            comentar.put("Comentario", comentario);

            sqLiteDatabase.insert("comentarios", null, comentar);

            sqLiteDatabase.close();
            edt_usuario.setText("");
            edt_comentario.setText("");

            Toast.makeText(this, "Comentario realizado", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Debes de llenar todo los campos", Toast.LENGTH_SHORT).show();
        }

    }
}
